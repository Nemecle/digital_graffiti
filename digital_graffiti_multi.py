#!/usr/bin/python
# Digital Graffiti - David Pride 2018. CC-BY License. Please feel free to distribute - but do give credit!
"""

shape menu:
  - up arrow cycles through round shapes:
      circle, ellipse, octagon
      it uses odd and positive values
  - left arrow cycles through squarey shapes:
      square, rectangle, trapezoid
      it uses even and negative values
  - right arrow cycles through other shapes:
      cow
      it uses odd and negative values
  - down arrow: cycles through orientation:
      see cycle_rotation() docstring

"""

import time
import os
import sys
import subprocess
import pygame
import cwiid
from pygame.locals import *

DEFAULT_WIDTH  = 1024
DEFAULT_HEIGHT = 768

# cwiid has a fixed resolution of 1024x768
# (https://github.com/pd-l2ork/cwiid/blob/master/libcwiid/cwiid.h#L161)
# must translate it to screen resolution
IR_WIDTH    = 1024
IR_HEIGHT   = 768

# to ease IR value reading
IRX = 0
IRY = 1

DRAW_SURFACE   = -1
CURSOR_SURFACE = -1

TIMING         = 0.2 #0.01
SPEED          = 1

white=(255,255,255)
red=(255,10,10)
green=(10,255,10)
dark_green=(10,102,10)
bright_green=(128,255,10)
blue=(10,10,255)
b_blue=(102,102,255)
pink=(255,0,255)
dark_pink=(153,0,76)
yellow=(255,255,51)
p_yellow=(204,255,153)
purple=(127,0,255)
brown=(102,51,0)
grey=(192,192,192)
taupe=(0,102,102)
black=(0,0,0)

colour_list = [red,green,dark_green,bright_green,blue,b_blue,pink,dark_pink,taupe, p_yellow, yellow,purple,brown,grey,black,white]

TRIANGLE = [(1,0),(-1,1),(-1,-1),(1,0)]
TRAPEZOID = [(1,-2), (2,1), (-2,1), (-1,-2)]
SEMICIRCLE = -1
# OH MAN! I totally forgot to bring an octagon
OCTAGON = [(3,1), (3,-1), (1,-3), (-1,-3), (-3,-1), (-3,1), (-1,3),( 1,3)]

COW_SHAPE = [(-4,0), (-3,-1), (2,-1), (1,-2), (5,-2), (4,-1), (4,-0), (2,-0), (2,1),
(1, 4), (1,1), (0,1), (0,2), (-1,1), (-2,2), (-2,1), (-4,4), (-3,0)]
#    \   ^__^
#     \  (oo)\_______
#        (__)\       )\/\
#            ||----w |
#            ||     ||

OWL_SHAPE_TOP =\
[(0,0), (0,-2), (2,-2), (3,-2), (3,-3), (2,-4), (3,-5), (2,-5), (1,-4), (-2,-4), (-3,-5),\
(-4,-5), (-3,-4), (-4,-3), (-4,-2),(-1,-2), (-1,0),(0,0)]
OWL_SHAPE_BOTTOM =\
[(0,0),(2,0),(2,-2),(3,-2),(3,1),(0,5),(-1,5),(-4,1),(-4,-2),(-3,-2),(-3,0),(-1,0),(0,2),(0,0)]

MAX_SIZE = 100

def get_screen_size():
    """
    return width, height of screen

    taken from
    http://www.blog.pythonlibrary.org/2015/08/18/getting-your-screen-resolution-with-python/

    """


    try:
        cmd = ['xrandr']
        cmd2 = ['grep', '*']
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        p2 = subprocess.Popen(cmd2, stdin=p.stdout, stdout=subprocess.PIPE)
        p.stdout.close()
         
        resolution_string, junk = p2.communicate()
        resolution = resolution_string.split()[0]
        width, height = resolution.split('x')
    except Exception as e:
        print("failed getting screen resolution: " + str(e))
        return -1

    return int(width), int(height)
    # return 1024, 768


def normalize(ni, min_n, max_n):
    """
    basic normalization function

    """

    n = (float(ni))
    
    try:
        nn = (n - min_n)/(max_n - min_n)
    except Exception as e:
        print("normalize: error while normalizing " + str(n) + ": " + str(e))
        return -1

    return nn


def get_semicircle(colour, size, orientation=0):
    """
    get a semicircle from PIL to blit on a pygame's surface
    colour      -- RGB tuple
    size        -- size of the equivalent circle
    orientation -- for later hypothetical use

    from
    https://stackoverflow.com/questions/41627322/drawing-semicircles-in-pygame

    """
    try:
        pil_image = Image.new("RGBA", (size, size))
        pil_draw = ImageDraw.Draw(image)
        pil_draw.arc((0, 0, size-1, size-1), 0, 270, fill=colour)
        # pil_draw.pieslice((0, 0, pil_size-1, pil_size-1), 330, 0, fill=GREY)

        mode = pil_image.mode
        pil_size = pil_image.size
        data = pil_image.tobytes()
        
        image = pygame.image.fromstring(data, pil_size, mode)
        image_rect = image.get_rect(center=screen.get_rect().center)

    except Exception as e:
        print("Error while generating semicircle: " + str(e))
        return -1

    return image, image_rect


def take_screenshot(draw_surface):
    """
    Take a screenshot of the drawing surface
    (old feature, moved to function)

    """

    image = pygame.Rect(0,0,width,1000)
    sub = draw_surface.subsurface(image)
    filename = "image%d.jpg" % img
    pygame.image.save(sub, filename)
    img += 1
    # print("Saved image%img.jpeg" % img)

    return 0


def cycle_rotation(x,y):
    """
    cycle the rotation of x and y invert factor
    for the down arrow feature

    step| x| y|
    ----+--+--+
      0 | 1| 1|
      1 |-1| 1|
      2 |-1|-1|
      3 | 1|-1|

    simpler flow is to be found

    """

    if x == 1 and y == 1:
        x = -1
    elif x == -1 and y == 1:
        y = -1
    elif x == -1 and y == -1:
        x = 1
    elif x == 1 and y == -1:
        y = 1
    else:
        print("cycle_rotation: whut (values: x:{},y:{})".format(str(x),
        str(y)))

    return x,y


class Wiimote:
    """
    wiimote wrapper

    xi -- x_invert, brush invertion on axis x
    yi -- y_invert, brush invertion on axis y

    """

    remote  = None
    c       = 0
    colour  = colour_list[0]
    size    = 10
    brush   = 1
    x1      = -100
    y1      = -100
    xi = 1
    yi = 1

    def __init__(self):
        """
        initialisation

        """

        return

    def pair_remote(self, mac_address, led=1, attempts=2):
        """
        pairing function

        """

        for attempt in range(0,attempts):
            try:
                print("attempt #{} for remote {}".format(attempt, mac_address))
                self.remote = cwiid.Wiimote(mac_address)
                self.remote.led=led

                if self.remote:
                    print("remote {} connected".format(mac_address))
                    break

            except RuntimeError:
                if (attempt >= attempts):
                    print("failed pairing remote " + mac_address)
                    return -1
                else:
                    print("failed pairing attempt #{} for remote" \
                    "{}".format(attempt, mac_address))
                    continue
            except Exception as e:
                print("Unknown exception: " + str(e))
                return -1

            time.sleep(0.5)

        try:
            self.remote.rpt_mode = cwiid.RPT_BTN | cwiid.RPT_ACC | cwiid.RPT_IR
            self.buttons = self.remote.state["buttons"]
        except Exception as e:
            print("Error while configuring remote: " + str(e))
            return -1

        return 0

    def check_state(self, draw_surface, cursor_surface):
        """
        check state of remote and act accordingly

        """

        width, height = draw_surface.get_size()
        pressed = False
        # buttons = selt.remote.state['buttons']
        # acc = self.remote.state
        # pos1 = acc['acc']
        button = self.remote.state['buttons']
        ir_sensor = self.remote.state['ir_src']
        # Uncomment to print out IR sensor readings
        #print ir_sensor

        ir_0 = None
        ir_1 = None

        
        if ir_sensor[0] != None:
            ir_0 = ir_sensor[0].get('pos')

        if ir_sensor[1] != None:
            ir_1 = ir_sensor[1].get('pos')


        if ir_0 != None:
            x_norm = int(normalize(ir_0[IRX], 0, IR_WIDTH) * width)
            y_norm = int(normalize(ir_0[IRY], 0, IR_HEIGHT) * height)
                                                                      
            self.x1 = width - self.size/2 - x_norm
            self.y1 = height - self.size/2 - y_norm

        else:
            self.x1 = -100
            self.y1 = -100


        # clusterfuck of a double IR sensor which isn't reliable,
        # one will do the job, screw that.
        # if ir_0 != None and ir_1 != None:
        #     # x_avg = (ir_0[IRX] + ir_1[IRX])/2
        #     # y_avg = (ir_0[IRY] + ir_1[IRY])/2

        #     x_avg = ir_0[IRX]
        #     y_avg = ir_0[IRY]

        #     x_norm = int(normalize(x_avg, 0, IR_WIDTH) * width)
        #     y_norm = int(normalize(y_avg, 0, IR_HEIGHT) * height)

        #     self.x1 = width - self.size/2 - x_norm
        #     self.y1 = height - self.size/2 - y_norm


        # if ir_0 != None and ir_1 == None:
        #     x_norm = int(normalize(ir_0[IRX], 0, IR_WIDTH) * width)
        #     y_norm = int(normalize(ir_0[IRY], 0, IR_HEIGHT) * height)

        #     self.x1 = width - self.size/2 - x_norm
        #     self.y1 = height - self.size/2 - y_norm


        # if ir_0 == None and ir_1 != None:
        #     x_norm = int(normalize(ir_1[IRX], 0, IR_WIDTH) * width)
        #     y_norm = int(normalize(ir_1[IRY], 0, IR_HEIGHT) * height)

        #     self.x1 = width - self.size/2 - x_norm
        #     self.y1 = height - self.size/2 - y_norm


        if self.x1 > width - self.size:
            self.x1 = width - self.size
        elif self.x1 < 0:
            self.x1 = 0 + self.size

        if self.y1 > height - self.size:
            self.y1 = height - self.size
        elif self.y1 < 0:
            self.y1 = 0 + self.size
   


        if button == cwiid.BTN_A and not pressed:
            pygame.draw.circle(draw_surface, white, (self.x1,self.y1), self.size)
            pressed = True
    
        if button == cwiid.BTN_2:
            pygame.quit()
    
        if button == cwiid.BTN_PLUS and not pressed:
            if self.size == MAX_SIZE:
                self.size = MAX_SIZE
            else:
                self.size += 1
            pressed = True
            
            
        if button == cwiid.BTN_MINUS and not pressed:
            if self.size == 2:
                self.size = 2
            else:
                self.size -= 1
            pressed = True     
            
            
        if button == cwiid.BTN_UP and not pressed:
            if self.brush < 0 or (self.brush % 2) == 0 or self.brush == 5:
                self.brush = 1
            else:
                self.brush += 2
            time.sleep(TIMING)
            
        if button == cwiid.BTN_DOWN and not pressed:
            self.xi, self.yi = cycle_rotation(self.xi, self.yi)
            time.sleep(TIMING)


            pass

        if button == cwiid.BTN_RIGHT and not pressed:
            if self.brush < 0 or (self.brush % 2) != 0 or self.brush == 4:
                self.brush = 2
            else:
                self.brush += 2
            time.sleep(TIMING)

        if button == cwiid.BTN_LEFT and not pressed:
            if self.brush > 0 or (self.brush % 2) != 0 or self.brush == -6:
                self.brush = -2
            else:
                self.brush -= 2
            time.sleep(TIMING)


        if button == cwiid.BTN_HOME and not pressed:
            if self.c < 15:
                self.c += 1
            else:
                self.c = 0
            self.colour = colour_list[self.c]
            pressed = True
            time.sleep(TIMING)

        # reset screen
        if button == cwiid.BTN_1 and not pressed:
            draw_surface.fill((255,255,255))
            # pygame.display.update()
            time.sleep(TIMING)
    
        # draw with B
        # see file docstring for explanation of values and logic
        if button == cwiid.BTN_B:
            if self.brush == 1: # circle
                pygame.draw.circle(draw_surface, self.colour, (self.x1,self.y1), self.size)
            elif self.brush == -2: # square
                pygame.draw.rect(draw_surface, self.colour,
                (self.x1,self.y1,self.size,self.size))
            elif self.brush == 2: # cow
                pygame.draw.polygon(draw_surface, self.colour,
                [(self.xi*x*self.size+self.x1, self.yi*y*self.size+self.y1) for x,y in COW_SHAPE])
                time.sleep(TIMING)
            elif self.brush == 3:
                pygame.draw.ellipse(draw_surface, self.colour,
                (self.x1, self.y1, self.size, 2*self.size))
                # (self.x1, self.y1, self.xi*self.size, self.yi*2*self.size))
            elif self.brush == 5:
                pygame.draw.polygon(draw_surface, self.colour,
                [(x*self.size/2+self.x1, y*self.size/2+self.y1) for x,y in OCTAGON])
            elif self.brush == -4:
                pygame.draw.rect(draw_surface, self.colour,
                (self.x1,self.y1,self.xi*self.size,self.yi*self.size*2))
            elif self.brush == -6:
                pygame.draw.polygon(draw_surface, self.colour,                                     
                [(self.xi*x*self.size+self.x1, self.yi*y*self.size+self.y1) for
                x,y in TRIANGLE])
            elif self.brush == -8:
                pygame.draw.polygon(draw_surface, self.colour,                                     
                [(self.xi*x*self.size+self.x1, self.yi*y*self.size+self.y1) for x,y in TRAPEZOID])
            elif self.brush == 4:
                pygame.draw.polygon(draw_surface, self.colour,
                [(self.xi*x*self.size+self.x1, self.yi*y*self.size+self.y1) for
                x,y in OWL_SHAPE_BOTTOM])
                pygame.draw.polygon(draw_surface, self.colour,
                [(self.xi*x*self.size+self.x1, self.yi*y*self.size+self.y1) for
                x,y in OWL_SHAPE_TOP])
                time.sleep(TIMING)
            else:
                print("drawing: unknown brush id. Current brush id: " +
                str(self.brush))
    
   
        # Cursor
        if self.brush == 1: # circle
            pygame.draw.circle(cursor_surface, self.colour, (self.x1,self.y1), self.size)
        elif self.brush == -2: # square
            pygame.draw.rect(cursor_surface, self.colour,
            (self.x1,self.y1,self.size,self.size))
        elif self.brush == 2: # cow
            pygame.draw.polygon(cursor_surface, self.colour,
            [(self.xi*x*self.size+self.x1, self.yi*y*self.size+self.y1) for x,y in COW_SHAPE])
        elif self.brush == 3:
            pygame.draw.ellipse(cursor_surface, self.colour,
            (self.x1,self.y1,self.size,2*self.size))
            # (self.x1,self.y1,self.xi*self.size,self.yi*2*self.size))
        elif self.brush == 5:
            pygame.draw.polygon(cursor_surface, self.colour,
            [(x*self.size/2+self.x1, y*self.size/2+self.y1) for x,y in OCTAGON])
        elif self.brush == -4:
            pygame.draw.rect(cursor_surface, self.colour,
            (self.x1,self.y1,self.xi*self.size,self.yi*self.size*2))
        elif self.brush == -6:
            pygame.draw.polygon(cursor_surface, self.colour,                                     
            [(self.xi*x*self.size+self.x1, self.yi*y*self.size+self.y1) for
            x,y in TRIANGLE])
        elif self.brush == -8:
            pygame.draw.polygon(cursor_surface, self.colour,                                     
            [(self.xi*x*self.size+self.x1, self.yi*y*self.size+self.y1) for x,y in TRAPEZOID])
        elif self.brush == 4:
            pygame.draw.polygon(cursor_surface, self.colour,
            [(self.xi*x*self.size+self.x1, self.yi*y*self.size+self.y1) for
            x,y in OWL_SHAPE_BOTTOM])
            pygame.draw.polygon(cursor_surface, self.colour,
            [(self.xi*x*self.size+self.x1, self.yi*y*self.size+self.y1) for
            x,y in OWL_SHAPE_TOP])
        else:
            print("drawing: unknown brush id. Current brush id: " +
            str(self.brush))


        return


def main():
    """
    main game loop

    """

    # init game
    pygame.init()
    pygame.event.set_blocked(pygame.MOUSEMOTION)    
    pygame.mouse.set_visible(0)
    mainloop = True

    clock = pygame.time.Clock()
    clock.tick(60)
    
    font = pygame.font.SysFont('Courier', 24)

    # init splash screen
    screen_resolution = get_screen_size()

    if screen_resolution is not -1:
        width, height = screen_resolution
    else:
        print("Falling back to default 1024x768 resolution")
        width  = DEFAULT_WIDTH
        height = DEFAULT_HEIGHT
        
    print("resolution is {}x{}, of type {} {}".format(width,height,
    type(width), type(height)))
    CURSOR_SURFACE = pygame.display.set_mode((width, height), pygame.FULLSCREEN)
   
    DRAW_SURFACE = CURSOR_SURFACE.copy()
    
    CURSOR_SURFACE.fill((34,34,34))
    pygame.display.update()
    pygame.event.set_blocked(pygame.MOUSEMOTION)
    pygame.mouse.set_visible(0)
    
    
    CURSOR_SURFACE.fill((34,34,34))
    pygame.display.update()
    digi = pygame.image.load('digital.jpeg')
    CURSOR_SURFACE.blit(digi, (475,400))
    pygame.display.update()
    time.sleep(1)
    
    CURSOR_SURFACE.fill((34,34,34))
    pygame.display.update()
    digi = pygame.image.load('connect.jpeg')
    CURSOR_SURFACE.blit(digi, (400,220))
    pygame.display.update()
    time.sleep(1)


    wm1 = Wiimote()
    wm2 = Wiimote()

    # init remote(s)
    wm1.pair_remote("00:1C:BE:43:9F:35", 1, 2)
    if wm1.remote:
        print("remote #1 connected")

    wm2.pair_remote("00:1F:C5:12:B4:2F", 2, 2)
    if wm2.remote:
        print("remote #2 connected")

    if not wm1.remote and not wm2.remote:
        print("no remote found: exiting")
        quit()

    CURSOR_SURFACE.fill((255,255,255))
    DRAW_SURFACE.fill(white)
    pygame.display.update()

    # game loop
    while mainloop:
        CURSOR_SURFACE.fill(white)
        CURSOR_SURFACE.blit(DRAW_SURFACE, (0, 0))

        if wm1.remote:
            wm1.check_state(DRAW_SURFACE, CURSOR_SURFACE)
        if wm2.remote:
            wm2.check_state(DRAW_SURFACE, CURSOR_SURFACE)

        pygame.display.update()

    pygame.quit()
    return 0

if __name__ == "__main__":
    main()
