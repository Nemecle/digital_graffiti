# digital_graffiti
## Introduction
*Based on https://github.com/davejavu1969/digital_graffiti.git*

This program allows to use Wiimotes as spray cans using mirrors 
to for vertical handling (see [original
project](http://www.piandchips.co.uk/uncategorized/digital-graffiti/)).

It also allows to use multiple remotes at the same time

## Controls (different from the original project):

| control       | Action                     | Comment                                                                                                                      |
| ------------- | -------------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| aim           | move brush                 | without a mirror on top, the y axis is inverted                                                                              |
| B (back)      | draw                       | special shapes freeze the programm for half a second to act as stamps                                                        |
| A             | erase                      | -                                                                                                                            |
| - and +       | change brush size          | -                                                                                                                            |
| home          | change brush colour        | in order: red,green,dark_green,bright_green,blue,b_blue,pink,dark_pink,taupe, p_yellow, yellow,purple,brown,grey,black,white |
| up arrow      | select shape for brush     | in order: circle, ellipse, octagon                                                                                           |
| down arrow    | select shape for brush     | in order: square, rectangle, triangle, trapeze                                                                               |
| left arrow    | select shape for brush     | in order: cow, owl (act as stamps, game freeze for a second)                                                                 |                  
| right arrow   | change brush's orientation | -                                                                                                                            |
| 1             | reset canvas               | -                                                                                                                            |
| 2             | leave program              | -                                                                                                                            |


## Installation

Follow [original instructions](http://www.piandchips.co.uk/uncategorized/digital-graffiti/) but with this repo instead

## Some notes regarding the project
I added "multiplayer", allowing two remotes (and with a few tweaks, 
virtually as many as a Wii IR reader allows), and added more brushes, including
animals given that this project was made for the *fête des lumières* (light
festival) at the *Salle des Rancy*, whose theme this year was animals.

I also cleaned the code a bit to remove some bugs and make it easier to expand
(given that I don't know the cwiid library and that documentation mostly
disappeared since it is apparently no longer maintained). Amongst other things,
the program can now detect screen resolution instead of using hardcoded values.

The program runs with python 2.7 because the library isn't available
on python 3.x